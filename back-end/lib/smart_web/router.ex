defmodule SmartWeb.Router do
  use SmartWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", SmartWeb do
    pipe_through :api
  end
end
